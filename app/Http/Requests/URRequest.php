<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class URRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'full_name' => 'required|string|max:50',
            'email' => 'required|email|unique:users|max:50',
            'password' => 'required||min:6|max:50',
            'uuid' => 'unique:users'
        ];
        return $rules;
    }
}
